{
	"story_1": "The world had its protective shield,",
	"story_2": "However, one day it had to yield,",
	"story_3": "As the infernal depths were unsealed.",
	
	"story_4": "The vile invasion did start,",
	"story_5": "From the depths the monsters depart,",
	"story_6": "To tear the whole world apart.",
	
	"story_7": "As most of humankind flee,",
	"story_8": "Masters of the elements three,",
	"story_9": "Stand brave to set humanity free.",
	
	"story_10": "For in a world that's gone amiss,",
	"story_11": "They travel through those gates of Dis,",
	"story_12": "To conquer it, that 「Elemental Abyss」.",
	
	"talk_tutor_1": "Oh, a new face. The [[$_abyss]] must have opened up in another world again.",
	"talk_tutor_2": "To seal this place you must defeat the [[satan]] in the center of the [[$_abyss]].",
	"talk_tutor_3": "I can see the determination in your eyes. I actually believe you can do this. Good luck.",

	//1:berserk|cleric|elf 2:goth|fairy|cowboy 3:pirate|monkey|gunner 4:sparta|diviner|ninja
	"talks_start1": "I knew you would return. You can do this.", 
	"talks_start2": "So you finally dared to show up?",
	"talks_start3": "Don't dawdle. The [[satan]] needs to be taken care of.",
	"talks_start4": "Hurry up! The world is still at the mercy of the monsters.",

	"ending_1": "Victory!",
	"ending_2": "The [[satan]] has been defeated. ",
	"ending_3": "The world is yet again at peace, albeit a temporary one.",
	"ending_4": "Thanks for playing!",
	
	"tartarus_word0": "It's admirable that a mere mortal has managed to get this far.",
	"tartarus_word1": "However, that begs the question: is that all you got?",
	"tartarus_word2": "Because the only thing you have accomplished here is to catch my attention.",
	"tartarus_word3": "In the end the [[$_abyss]] will devour everything. What you've done here ultimately amounts to nothing.",
	"tartarus_word4": "Still, you're the first human to ever reach me. I'll admit, I'm quite impressed. Amused might be a better word, actually.",
	"tartarus_word5": "Anyways, it's time to hit the road. I'm looking forward to our next meeting, if you survive that is.",


	"talk_normal_win": "Although this means nothing, in the grand scheme of things, it's still quite interesting that humans dare to confront the gods."
}