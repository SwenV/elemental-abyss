{
	"home_startgame": "Play",
	"home_newgame": "New Game",
	"home_continuegame": "Continue",
	"home_cardbook": "Essence",
	"home_option": "Options",
	"home_develop": "Credits",
	"home_exit": "Quit",
	
	"lost_title": "You fall into a coma...",
	"victory_title": "You've saved the world!",
	"leave_abyss": "Leave",
	"history_title": "History",
	"result_title": "Result",
	"card_name": "Essence",

	"success": "Success",
	"fail": "Failure",

	"difficult_0": "Normal Difficulty",
	"difficult_1": "Nightmare Difficulty",

	"history_hint": "No history",	

	"finish_stage": "Encounters Won",
	"finish_monster": "Enemies Slain",
	"finish_elite": "Elites Killed",
	"finish_boss": "Bosses Defeated",
	"finish_mission": "Tasks Completed",
	"finish_point": "Final Score",
	
	"cardbook_title": "Essence Collection",
	"cardbook_normal": "Normal",
	"cardbook_monk": "[[class_monk]]",
	"cardbook_mage": "[[class_mage]]",
	"cardbook_archer": "[[class_archer]]",
	
	"button_option": "Option",
	"button_tutorial": "Tutorial",
	"button_cardbook": "Essence Collection",
	
	"newcontentdesc_card": "{0}",
	"newcontentdesc_set": "{0}",
	"newcontentdesc_stone": "New Path",
	"newcontentdesc_selectstart": "Entrance Selection",
	"newcontents": "New Contents",
	"new": "new",
	"newcontentdesc_stone_desc": "Unlock a new path in the [[$_abyss]]",
	"newcontentdesc_selectstart_desc": "You may choose which entrance of the [[$_abyss]] to start from",
	"newcontentdesc_refresh": "Redraw × {0}",
	"newcontentdesc_refresh_desc": "You may choose to redraw your cards before combat",
	"map_stone": "New Path",
	
	"confirm_restart": "New Game",
	"confirm_continue": "Continue Game",
	"confirm_title": "Do you want to continue your current adventure?",
	"confirm_hint": "Your current adventure will be lost, this can't be undone.",
	"confirm_newgame": "Start a new game?",
	"confirm_newhint": "Current game progress will be permanently erased",
	"yes": "Yes",
	"no": "No"
}