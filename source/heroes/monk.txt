{
	"monk_name": "Monk",
	"monk_talent": "Mental Strength",
	"monk_skill_label": "[[monk_talent]]",
	
	"monk_desc": "[[monk_talent]]: If you have armor, you can't suffer more than 2 damage.",
	
	"monk_up0": "HP +6",
	"monk_up1": "Start with 「[[zhuanzhu]]」",
	"monk_up2": "Awaken 「[[monk_talent]]」",	
	
	"monk_stagestart": "Destiny brought me here",
	"monk_useskill": "Seize the moment!",
	"monk_armorbroken": "I can't escape my destiny!",
	"monk_killboss": "Lay down your blade, and you'll find peace!",
	"monk_revive": "My quest is not yet over!",
	"monk_meetelite": "That foul stench!",
	"monk_skill2monsters": "This is what you deserve!",
	"monk_behurt": "Hardship will not deter me",
	
	
	"event_monk_1": "You see a man in the distance. He's kneeling next to a lifeless body, clasping his hands in prayer.",
	"event_monk_2": "I am praying for her, may her soul find its way to the far beyond.",
	"event_monk_3": "Pray for her",
	"event_monk_4": "Check her pockets",
	"event_monk_5": "As you kneel down in prayer, you feel as if a heavy burden has been lifted from your shoulders. You rise to your feet fairly content, given the circumstances.",
	"event_monk_6": "You find some scrolls in her pockets. The [[monk_name]] doesn't seem to notice as you quickly snatch them.",
	"event_monk_7": "[[event_monk_6]] However, as you step away you feel quite weak. You start to regret getting too close to the infected corpse."	
}