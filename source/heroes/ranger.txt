{
	"ranger_name": "Ranger",
	"ranger_talent": "Rapid Barrage",
	"ranger_skill_label": "[[ranger_talent]]",
	
	"ranger_desc": "[[ranger_talent]]: For each unused step at the end of turn, attack again.",

	"ranger_up0": "Start with 「[[tegong]]」",
	"ranger_up1": "Start with 「[[xinghongjianyu]]」",
	"ranger_up2": "Awaken 「[[ranger_talent]]」",	
	
	"ranger_stagestart": "I hear the trees whispering",
	"ranger_useskill": "This arrow has your name on it",
	"ranger_armorbroken": "Time to show you my real skills",
	"ranger_killboss": "Nothing escapes my arrows",
	"ranger_revive": "You can't succeed without making a few mistakes",
	"ranger_meetelite": "That's scary looking",
	"ranger_skill2monsters": "I can't miss at this distance!",
	"ranger_behurt": "Ouch, that really hurt",
	
	"event_ranger_1": "You spot a strange character squatting next to the road. As you approach you can see her studying some peculiar herb.",
	"event_ranger_2": "Did you know these herbs can help you with all sorts of ailments? I've brewed a lot of different potions from them. Here, have one, I got way more than I need.",
	"event_ranger_3": "The orange potion",
	"event_ranger_4": "The purple potion",
	"event_ranger_5": "Politely decline",
	"event_ranger_6": "Don't drink it all at once. Have a nice day.",
	"event_ranger_7": "Don't drink it all at once! Now you're probably going to be sick.",
	"event_ranger_8": "Your mother always taught you not to accept weird potions from strangers. You leave the weird woman behind, feeling relieved nothing bad happened."
}